const express = require('express')
const axios = require('axios').default
const parser = require('node-html-parser').parse
const app = express()
const PORT = process.env.PORT || 8080

app.get('/', function(req, res){
    // query string
    var query = req.query.consulta

    // gerar um objeto
    var cursos = []

    //buscar os dados
    var url = "http://matriculas.unesc.net/graduacao"
    var requisicao = axios.get(url)
    requisicao.then(function(resposta){
        //tratar os dados
        var root = parser(resposta.data)
        var divCursos = root.querySelectorAll(".curso__item")

        divCursos.forEach(function(curso){
            var c = {
                "nome": curso['_attrs']['data-title'],
                "tipo": curso['_attrs']['data-type'],
                "timestamp": Date.now()
            }
            
            if(c.nome.toUpperCase().includes(query.toUpperCase())){
                cursos.push(c)
            }
        })

        //entregar a resposta
        res.json(cursos)
    })
})

app.get('*', function(req, res){
    res.redirect('http://professor.venson.net.br')
})

app.listen(PORT, function(){
    console.log("Servidor iniciado")
})